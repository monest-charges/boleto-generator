# boleto-generator


Biblioteca em Node.js para geração de boletos utilizando PDFKit baseada e modificada a partir do [gerador-boletos](https://npmjs.org/package/gerador-boletos) para atender as minhas necessidades

Geração de boletos para bancos:
- Bradesco
- Caixa
- Ailos (Cecred)
- Itaú
- Sicoob
- Sicredi
- Santander
- Banco do Brasil

### Install

```javascript
npm i boleto-generator
```
### Exemplos de uso

* [aqui](/examples)

### Run tests

```javascript
npm run test
```


