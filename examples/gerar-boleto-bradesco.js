import Gerador from '../index';
import { gerarPdf, gerarBoleto} from './index';
import streamToPromise from '../lib/utils/util';


const boleto = {
	banco: new Gerador.boleto.bancos.Bradesco(),
	pagador: { RegistroNacional: '06276420980' },
	beneficiario: {
		dadosBancarios:{
			carteira: '09',
			agencia: '3645',
			agenciaDigito: '5',
			conta: '0006425',
			contaDigito: '4' ,
			nossoNumero: '350000004',
		}
	},
	boleto: {
		numeroDocumento: '10',
		especieDocumento: 'DS',
		valor: 1.00,
		datas: {
			vencimento: '04/02/2020',
			processamento:  '04/02/2020',
			documentos: '04/02/2020'
		}
	}
};
// 23793.64504 40034.000006 28000.642505 1 81540000000100
	
const novoBoleto = gerarBoleto(boleto);
gerarPdf(novoBoleto).then(async({stream})=>{
	// ctx.res.set('Content-type', 'application/pdf');	
	await streamToPromise(stream);
}).catch((error)=>{
	return error;
});
